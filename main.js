students = ['Eden', 'Refael', 'Yoni', 'Nitzan', 'Hadas'];
attendees = [['Eden', 'Refael', 'Yoni', 'Nitzan', 'Hadas', 'Ortal'], 
 ['Berry', 'Nitzan', 'Yoni', 'Eden', 'Hadas', 'Ortal'], 
 ['Maxim', 'Ortal', 'Yoni', 'Refael', 'Nitzan', 'Alex'], 
            ['Eden', 'Andrew', 'Yoni', 'Nitzan', 'Ortal','Nitzan']];
N = 3;	

function topNStudentsAttendees(students, attendees, N) {
    if (!Number.isInteger(N) || N <= 0){
        console.log("Please choose a positive round number!");
        return;
    }
    
    if (!Array.isArray(students)) {
        console.log("The studnet list is invalid");
        return;
    }
    
    if (!attendees.filter(Array.isArray).length) {
        console.log("The attendees list is invalid");
        return;
    }
    
    result = new Map(
        (function(){
            arr = [];
            while(students.length)
                arr.push([students.splice(0,1)[0], 0]);
            return arr;
        })());
    
    for (i = 0; i < attendees.length; i++) {
        uniques = [...new Set(attendees[i])];
        for (j = 0; j < uniques.length; j++) {
            if (result.has(uniques[j])) {
                result.set(uniques[j], result.get(uniques[j]) + 1);
            }
        }
    }
    return [...result.entries()].sort((a, b) => b[1] - a[1]).slice(0, N);
}

console.log(topNStudentsAttendees(students, attendees, N));